﻿# Hello Algorithm

## Find-Missing Number

```powershell

PS C:\Workspace\hello-space\helloalgorithms> Get-Help Find-MissingNumber -Detailed

DESCRIPTION
    You have an array of n incrementing numbers. One is missing.
    Find out which one but in a linear fashion and with as little
    strain on memory as possible.    

PARAMETERS
    -Numbers <Int32[]>
            
    -------------------------- EXAMPLE 1 --------------------------
    
    PS C:\>Result: 6
    
    Find-MissingNumber -Numbers @(1,2,3,5,6)
```

![](./Find-MissingNumber.gif)

