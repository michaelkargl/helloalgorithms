﻿<#
  .SYNOPSIS 
  Find the missing number

.DESCRIPTION
  You have an array of n incrementing numbers. One is missing.
  Find out which one but in a linear fashion and with as little
  strain on memory as possible.

.EXAMPLE 
    Find-MissingNumber -Numbers @(1,2,3,5,6)
    Result: 6
#>
Function Find-MissingNumber {
    PARAM(
        [Parameter(Mandatory)]
        [int[]]$Numbers
    )

    BEGIN {
        Write-Debug "Begin: $($MyInvocation.MyCommand.Name)"
    }

    PROCESS {
        $Boundries = $Numbers | Measure-Object -Maximum -Minimum
        
        # Applying gausian sum formular for calculating the sum of n integers 
        # http://mathandmultimedia.com/2010/09/15/sum-first-n-positive-integers/
        $n = $Boundries.Maximum
        $GaussSum = ($n * ($n + 1)) / 2

        # Calculate the actual sum
        $Sum = $Numbers | Measure-Object -Sum | Select-Object -ExpandProperty Sum

        # The difference must be the missing number
        $GaussSum - $Sum
    }

    END {
        Write-Debug "End: $($MyInvocation.MyCommand.Name)"
    }
}


Find-MissingNumber -Numbers @(1,2,3,5,6)